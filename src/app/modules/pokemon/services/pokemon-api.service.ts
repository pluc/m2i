import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { TOKEN_POKEMON_API_URL } from '@pokemon/api-url';
import { Pokemons, EquipeAvecResume, ResumeEquipe, PokemonsAndTypes, PokemonTypes } from '@pokemon/entities';
import { Observable, catchError, forkJoin, map, mergeMap, of } from 'rxjs';

/**
 * Implémentation d'appels à l'API https://pokebuildapi.fr/api/v1
 */
@Injectable()
export class PokemonAPIService {

  constructor(
    @Inject(TOKEN_POKEMON_API_URL) private pokemonApiUrl: string,
    private httpClient: HttpClient) {

     }

  public getAllPokemons(): Observable<Pokemons> {
    let allPokemons: Pokemons;

    return this.httpClient.get<Pokemons>(`${this.pokemonApiUrl}/pokemon`).pipe(
      mergeMap((pokemons: Pokemons) => {
        allPokemons = pokemons;

        const cries: Observable<string>[] = [];

        for (const pokemon of pokemons) {
          const cry$: Observable<string> = this.getPokemonCry(pokemon.id);
          cries.push(cry$);
        }

        // forkJoin permet de transformer un tableau d'observable en un observable qui contient un tableau
        const cries$: Observable<string[]> = forkJoin(cries);

        return cries$;
      }),
      map((cries: string[]) => {
        for (let i = 0; i < allPokemons.length; i++) {
          allPokemons[i].cry = cries[i];
        }

        return allPokemons;
      }),
      catchError(e => {
        console.error(e);
        return of([]);
      })
    )
  }

  public playAllCries(pokemons: Pokemons): void {
    let i = 0;

    const execute = () => {
      pokemons[i].isPlaying = true;
      const audio = new Audio(pokemons[i].cry);
      audio.play().then(() => {

        audio.addEventListener('ended', () => {
          pokemons[i].isPlaying = false;

          i++;
          if (i < pokemons.length) {
            execute();
          }
        })

      })
    }

    execute();
  }

  public getRandomTeamWithResume(): Observable<EquipeAvecResume> {
    let teamPokemon: Pokemons;

    // 1er appel pour récupérer une team aléatoire
    return this.httpClient.get<Pokemons>(`${this.pokemonApiUrl}/random/team`).pipe(
      // On fait un mergeMap pour pouvoir exécuter ensuite un 2eme observable
      mergeMap((pokemons: Pokemons) => {

        teamPokemon = pokemons;

        const body: any[] = pokemons.map(p => {
          const pokemon: any = {};
          pokemon[p.id] = null;
          return pokemon;
        })

        // 2eme appel pour récupérer le résumé de l'équipe
        return this.httpClient.post<ResumeEquipe>(`${this.pokemonApiUrl}/team/defensive-coverage/v2`, body);
      }),
      // On fait un map pour retourner un objet qui contient le résultat des 2 appels
      map((resumeEquipe: ResumeEquipe) => {
        return <EquipeAvecResume>{
          pokemons: teamPokemon,
          resumes: resumeEquipe
        }
      }),
      catchError(e => {
        console.error(e);
        return of({} as EquipeAvecResume)
      })
    )
  }

  public getAllPokemonAndAllTypes(search?: string): Observable<PokemonsAndTypes> {
    const result = <PokemonsAndTypes>{};

    return this.getAllPokemons().pipe(
      mergeMap((pokemons: Pokemons) => {
        result.pokemons = pokemons.filter(p => search == null || p.name.toLowerCase().indexOf(search.toLowerCase()) >= 0);

        return this.getAllPokemonTypes();
      }),
      map((pokemonTypes: PokemonTypes) => {
        result.types = pokemonTypes.filter(t => search == null || t.name.toLowerCase().indexOf(search.toLowerCase()) >= 0);

        return result;
      }),
      catchError(e => {
        console.error(e);
        return of(result);
      })
    )
  }

  public getPokemonCry(id: number): Observable<string> {
    return this.httpClient.get("https://pokeapi.co/api/v2/pokemon/" + id).pipe(
      map((o: any) => {
        return o?.cries?.latest;
      })
    )
  }

  public getAllPokemonTypes(): Observable<PokemonTypes> {
    return this.httpClient.get<PokemonTypes>(`${this.pokemonApiUrl}/types`);
  }

  public getAllPokemonOfType(type: unknown): Observable<unknown> {
    throw "Not implemented";
  }
}
