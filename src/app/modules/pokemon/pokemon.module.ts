import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FichePokemonComponent } from './components/fiche-pokemon/fiche-pokemon.component';
import { PokedexComponent } from './components/pokedex/pokedex.component';
import { PokemonAPIService } from './services/pokemon-api.service';
import { PokemonRoutingModule } from './pokemon.routing.module';
import { HttpClientModule } from '@angular/common/http';
import { EquipeAleatoireComponent } from './components/equipe-aleatoire/equipe-aleatoire.component';
import { FormsModule } from '@angular/forms';
import { TOKEN_POKEMON_API_URL } from './api-url';

@NgModule({
  declarations: [
    FichePokemonComponent,
    PokedexComponent,
    EquipeAleatoireComponent
  ],
  exports: [
    PokedexComponent,
    EquipeAleatoireComponent
  ],
  imports: [
    CommonModule,
    PokemonRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    { provide: TOKEN_POKEMON_API_URL, useValue: 'https://pokebuildapi.fr/api/v1' },
    PokemonAPIService
  ]
})
export class PokemonModule { }
