import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FichePokemonComponent } from './components/fiche-pokemon/fiche-pokemon.component';
import { PokedexComponent } from './components/pokedex/pokedex.component';
import { EquipeAleatoireComponent } from './components/equipe-aleatoire/equipe-aleatoire.component';

const routes: Routes = [
  { path: 'fiche/:numero', component: FichePokemonComponent },
  { path: 'random-team', component: EquipeAleatoireComponent },
  { path: '**', component: PokedexComponent }
]

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    RouterModule.forChild(routes)
  ],
})
export class PokemonRoutingModule { }
