import { AfterViewInit, Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';
import { PokemonsAndTypes } from '../../entities/pokemon-and-types';
import { Pokemon, Pokemons } from '../../entities/pokemons';
import { PokemonAPIService } from '../../services/pokemon-api.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrl: './pokedex.component.scss'
})
export class PokedexComponent implements AfterViewInit {

  @ViewChild(NgForm)
  private formElement?: NgForm;

  @ViewChild('search')
  private searchElement?: ElementRef<HTMLInputElement>;

  @ViewChildren('fichePokemon')
  private fichesPokemon?: QueryList<HTMLDivElement>;

  ngAfterViewInit(): void {
    console.log('Form', this.formElement);

    if (this.searchElement) {
      this.searchElement.nativeElement.value = 'toto';
    }

    console.log('fiches pokemon', this.fichesPokemon);
  }

  public pokemonsAndTypes$: Observable<PokemonsAndTypes>;

  constructor(public pokemonAPIService: PokemonAPIService) {
    this.pokemonsAndTypes$ = this.pokemonAPIService.getAllPokemonAndAllTypes();
  }

  public playAllSongs(pokemons: Pokemons): void {
    this.pokemonAPIService.playAllCries(pokemons);
  }

  public filter(search: string): void {
    this.pokemonsAndTypes$ = this.pokemonAPIService.getAllPokemonAndAllTypes(search);
  }

  public playPokemonCry(pokemon: Pokemon): void {
    new Audio(pokemon.cry).play();
  }
}
