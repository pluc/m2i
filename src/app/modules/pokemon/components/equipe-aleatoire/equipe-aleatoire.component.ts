import { Component } from '@angular/core';
import { PokemonAPIService } from '../../services/pokemon-api.service';
import { Observable } from 'rxjs';
import { EquipeAvecResume } from '../../entities/resume';

@Component({
  selector: 'app-equipe-aleatoire',
  templateUrl: './equipe-aleatoire.component.html',
  styleUrl: './equipe-aleatoire.component.scss'
})
export class EquipeAleatoireComponent {
  public randomTeam$: Observable<EquipeAvecResume>;

  constructor(private pokemonAPIService: PokemonAPIService) {
    this.randomTeam$ = this.pokemonAPIService.getRandomTeamWithResume();
  }
}
