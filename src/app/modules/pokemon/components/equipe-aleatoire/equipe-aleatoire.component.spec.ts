import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipeAleatoireComponent } from './equipe-aleatoire.component';

describe('EquipeAleatoireComponent', () => {
  let component: EquipeAleatoireComponent;
  let fixture: ComponentFixture<EquipeAleatoireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EquipeAleatoireComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EquipeAleatoireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
