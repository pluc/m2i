import { PokemonTypes } from "./pokemon-types";
import { Pokemons } from "./pokemons";

export interface PokemonsAndTypes {
    pokemons: Pokemons;
    types: PokemonTypes;
}