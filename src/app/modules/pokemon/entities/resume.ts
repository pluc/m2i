import { Pokemons } from "./pokemons";

export type ResumeEquipe = ResumeEquipeType[]

export interface ResumeEquipeType {
    name: string
    result: string
    message: string
}

export interface EquipeAvecResume {
    pokemons: Pokemons;
    resumes: ResumeEquipe;
}