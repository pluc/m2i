export * from './pokemon-and-types';
export * from './pokemon-types';
export * from './pokemons';
export * from './resume';