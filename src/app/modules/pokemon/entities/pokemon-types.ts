export type PokemonTypes = PokemonType[]

export interface PokemonType {
  id: number
  name: string
  image: string
  englishName: string
}
