import { InjectionToken } from "@angular/core";

export const TOKEN_POKEMON_API_URL = new InjectionToken('TOKEN_POKEMON_API_URL');