import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateUpdateProjetComponent } from './components/create-update-projet/create-update-projet.component';
import { ListProjetsComponent } from './components/list-projets/list-projets.component';

const routes: Routes = [
  { path: 'create', component: CreateUpdateProjetComponent },
  { path: 'update/:id', component: CreateUpdateProjetComponent },
  { path: '**', component: ListProjetsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
