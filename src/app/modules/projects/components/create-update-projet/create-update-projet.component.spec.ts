import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUpdateProjetComponent } from './create-update-projet.component';

describe('CreateUpdateProjetComponent', () => {
  let component: CreateUpdateProjetComponent;
  let fixture: ComponentFixture<CreateUpdateProjetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreateUpdateProjetComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreateUpdateProjetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
