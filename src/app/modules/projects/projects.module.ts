import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ListProjetsComponent } from './components/list-projets/list-projets.component';
import { CreateUpdateProjetComponent } from './components/create-update-projet/create-update-projet.component';


@NgModule({
  declarations: [ListProjetsComponent, CreateUpdateProjetComponent],
  imports: [
    CommonModule,
    ProjectsRoutingModule
  ]
})
export class ProjectsModule { }
