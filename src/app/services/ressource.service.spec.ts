import { TestBed } from '@angular/core/testing';

import { RessourceService } from './ressource.service';
import { Ressource } from '../entities/ressource';

describe('RessourceService', () => {
  let service: RessourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RessourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('La création d\'une ressource fonctionne', () => {
    const resource = new Ressource();
    resource.code = 'RES_1';
    resource.label = 'Ressource 1';

    const copy = new Ressource();
    Object.assign(copy, resource);

    expect(service.getAllRessources().length).toBe(0);

    expect(service.createRessource(resource)).toBeTrue();

    expect(resource.id).toBe(1);

    expect(service.getAllRessources().length).toBe(1);
  });
});
