import { Injectable } from '@angular/core';
import { Ressource } from '../entities/ressource';

@Injectable({
  providedIn: 'root'
})
export class RessourceService {

  private _ressources: Ressource[] = [];

  constructor() { }

  public createRessource(resource: Ressource): boolean {

    const maxId: number = this._ressources.length == 0 ? 0 :
      Math.max(...this._ressources.map(r => r.id ?? 0));

    resource.id = maxId + 1;

    this._ressources.push(resource);

    return true;
  }

  public updateRessource(resource: Ressource): boolean {
    if (resource == null) {
      return false;
    }

    const index: number = this._ressources.findIndex(r => r.id == resource.id);

    if (index < 0) {
      return false;
    }

    this._ressources[index] = resource;

    return true;
  }

  public deleteRessource(id: number): boolean {
    const index: number = this._ressources.findIndex(r => r.id == id);

    if (index < 0) {
      return false;
    }

    this._ressources.splice(index, 1);

    return true;
  }

  public getAllRessources(): Ressource[] {
    return this._ressources;
  }

  public getRessourceById(id: number): Ressource | undefined {
    return this._ressources.find(r => r.id == id);
  }
}
