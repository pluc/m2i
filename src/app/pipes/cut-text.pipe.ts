import { HostListener, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cutText',
  standalone: true
})
export class CutTextPipe implements PipeTransform {
  
  transform(value: string | undefined, ...args: any[]): string {
    if (value == null) {
      return '';
    }

    let limit: number = args != null && args.length > 0 ? args[0] as number : 3;

    if (!Number.isInteger(limit)) {
      limit = 3;
    }

    return value.substring(0, limit);
  }

}
