import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appAlert]',
  standalone: true
})
export class AlertDirective {

  @Input()
  public message = 'ALERT!!'; 

  constructor() { }

  @HostListener('click')
  public alertFunction(): void {
    alert(this.message);

  }

}
