import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[appConfirm]',
  standalone: true
})
export class ConfirmDirective {

  @Input() public message?: string

  @Output() public confirmed = new EventEmitter<void>();

  constructor() { }

  @HostListener('click')
  private clicked(): void {
    if (confirm(this.message)) {
      this.confirmed.emit();
    }
  }

}
