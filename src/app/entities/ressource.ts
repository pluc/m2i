export class Ressource {
    public id?: number;
    public code?: string;
    public label?: string
}
