import { Component } from '@angular/core';
import { CompteurService } from '../../services/compteur.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-date-du-jour',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './date-du-jour.component.html',
  styleUrl: './date-du-jour.component.scss'
})
export class DateDuJourComponent {
  public today: Date = new Date();

  constructor(public compteurService: CompteurService) { }
}
