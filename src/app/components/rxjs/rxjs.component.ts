import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-rxjs',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './rxjs.component.html',
  styleUrl: './rxjs.component.scss'
})
export class RxjsComponent {

  public n$: Observable<number>;

  constructor() {
    this.n$ = of(42);

    this.n$.subscribe((n: number) => {
      console.log('n=' + n);
    })
  }
}
