import { Component } from '@angular/core';
import { RessourceService } from '../../services/ressource.service';
import { Ressource } from '../../entities/ressource';
import { CommonModule } from '@angular/common';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { CreateUpdateRessourceComponent } from '../create-update-ressource/create-update-ressource.component';
import { CutTextPipe } from '../../pipes/cut-text.pipe';
import { ConfirmDirective } from '../../directives/confirm.directive';

@Component({
  selector: 'app-liste-ressources',
  standalone: true,
  imports: [CommonModule, MatDialogModule, CutTextPipe, ConfirmDirective],
  templateUrl: './liste-ressources.component.html',
  styleUrl: './liste-ressources.component.scss'
})
export class ListeRessourcesComponent {

  public ressources: Ressource[];

  constructor(private ressourceService: RessourceService, private matDialog: MatDialog) {
    this.ressources = this.ressourceService.getAllRessources();
  }

  public deleteRessource(ressource: Ressource): void {
    this.ressourceService.deleteRessource(ressource.id ?? -1);
  }

  public updateRessource(ressource?: Ressource): void {

    // Ouverture d'une boite de dialogue
    const dialog: MatDialogRef<CreateUpdateRessourceComponent, any> =
      this.matDialog.open(CreateUpdateRessourceComponent);

    // Binding de la ressource
    if (ressource != null) {
      dialog.componentInstance.ressource = ressource;
    }

    // Abonnement à l'évenement de post création de la ressource
    dialog.componentInstance.afterSubmitSuccess.subscribe(() => {
      dialog.close();
    });

    dialog.componentInstance.cancel.subscribe(()=> {
      dialog.close();
    })
  }

  public addRessource(): void {
    this.updateRessource();
  }
}
