import { Component } from '@angular/core';
import { PokemonModule } from '../../modules/pokemon/pokemon.module';
import { CompteurService } from '../../services/compteur.service';
import { DateDuJourComponent } from '../date-du-jour/date-du-jour.component';
import { ListeRessourcesComponent } from '../liste-ressources/liste-ressources.component';
import { ProgressBarComponent } from '../progress-bar/progress-bar.component';
import { SliderComponent } from '../slider/slider.component';
import { CommonModule } from '@angular/common';
import { AlertDirective } from '../../directives/alert.directive';

@Component({
  selector: 'app-demo',
  standalone: true,
  imports: [CommonModule, DateDuJourComponent, SliderComponent, ProgressBarComponent,
    ListeRessourcesComponent, AlertDirective,
    PokemonModule],
  templateUrl: './demo.component.html',
  styleUrl: './demo.component.scss'
})
export class DemoComponent {

  public sliderValue: number = 6;
  public sliderValue2 = 50;

  public numbers: number[] = [1, 2, 3];

  constructor(public compteurService: CompteurService) {

    setInterval(() => {
      this.sliderValue = Math.random() * 100;
      this.sliderValue2 = Math.random() * 100;

      this.compteurService.cpt++;

    }, 500);
  }
}
