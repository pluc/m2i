import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';

@Component({
  selector: 'app-slider',
  standalone: true,
  imports: [MatSliderModule, FormsModule],
  templateUrl: './slider.component.html',
  styleUrl: './slider.component.scss'
})
export class SliderComponent {

  @Input()
  public value: number = 0;

  @Output()
  public valueChange = new EventEmitter<number>();

  public emitValue(value: any): void {
    this.valueChange.emit(value);
  }
}
