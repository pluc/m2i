import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Ressource } from '../../entities/ressource';
import { RessourceService } from '../../services/ressource.service';
import { ConfirmDirective } from '../../directives/confirm.directive';

@Component({
  selector: 'app-create-update-ressource',
  standalone: true,
  imports: [FormsModule, ConfirmDirective],
  templateUrl: './create-update-ressource.component.html',
  styleUrl: './create-update-ressource.component.scss'
})
export class CreateUpdateRessourceComponent implements OnInit {

  @Input()
  public ressource = new Ressource();

  @Output()
  public afterSubmitSuccess = new EventEmitter<void>();

  @Output()
  public cancel = new EventEmitter<void>();

  public get isInCreation(): boolean {
    return this.ressource.id == null;
  }

  constructor(private ressourceService: RessourceService) {
  }

  ngOnInit(): void {
    // On clone la ressource reçue en entrée, pour modifier non pas l'objet du service
    // mais une copie de cet objet
    const clone = new Ressource();

    Object.assign(clone, this.ressource);

    this.ressource = clone;
  }

  public submit(): void {
    let success = false;

    if (this.isInCreation) {
      success = this.ressourceService.createRessource(this.ressource);
    } else {
      success = this.ressourceService.updateRessource(this.ressource);
    }

    if (success) {

      // émission d'une information que le submit a été réalisé avec succès
      this.afterSubmitSuccess.emit();
    }
  }
}
