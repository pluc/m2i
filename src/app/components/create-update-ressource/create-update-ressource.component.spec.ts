import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUpdateRessourceComponent } from './create-update-ressource.component';

describe('CreateUpdateRessourceComponent', () => {
  let component: CreateUpdateRessourceComponent;
  let fixture: ComponentFixture<CreateUpdateRessourceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreateUpdateRessourceComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreateUpdateRessourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
