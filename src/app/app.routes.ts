import { Routes } from '@angular/router';
import { ListeRessourcesComponent } from './components/liste-ressources/liste-ressources.component';
import { RxjsComponent } from './components/rxjs/rxjs.component';
import { CanActivatePokemonModule } from '@pokemon/can-activate';

export const routes: Routes = [
    {
        path: 'pokedex',
        loadChildren: () => import('./modules/pokemon/pokemon.module').then(f => f.PokemonModule),
        canActivate: [CanActivatePokemonModule()]
    },
    
    { path: 'ressources', component: ListeRessourcesComponent },
    {
        path: 'projets',
        loadChildren: () => import('./modules/projects/projects.module').then(f => f.ProjectsModule)
    },
    { path: 'rxjs', component: RxjsComponent },
    {
        path: '**',
        loadComponent: () => import('./components/demo/demo.component').then(f => f.DemoComponent)
    }
];
